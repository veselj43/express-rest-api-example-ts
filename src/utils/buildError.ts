import HttpStatus from 'http-status-codes';
import Boom from 'boom';
import Joi from 'joi';

interface ErrorDetail {
    param: string;
    message: string;
}

interface ErrorResponse {
    code: number;
    message: string;
    details?: Array<ErrorDetail>;
}

/**
 * Build error response for validation errors.
 */
function buildError(err: Joi.ValidationError | Boom | any): ErrorResponse {
    // Validation errors
    if (err.isJoi) {
        return {
            code: HttpStatus.BAD_REQUEST,
            message: HttpStatus.getStatusText(HttpStatus.BAD_REQUEST),
            details:
                err.details &&
                err.details.map((err: Joi.ValidationErrorItem) => {
                    return {
                        message: err.message,
                        param: err.path.join('.'),
                    };
                }),
        };
    }

    // HTTP errors
    if (err.isBoom) {
        return {
            code: err.output.statusCode,
            message: err.output.payload.message || err.output.payload.error,
        };
    }

    // Return INTERNAL_SERVER_ERROR for all other cases
    return {
        code: HttpStatus.INTERNAL_SERVER_ERROR,
        message: HttpStatus.getStatusText(HttpStatus.INTERNAL_SERVER_ERROR),
    };
}

export default buildError;
