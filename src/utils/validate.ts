import Joi from 'joi';

/**
 * Utility helper for Joi validation.
 */
function validate(data: Object, schema: Object, options: Object = {}): Promise<Joi.ValidationError | null> {
    return Joi.validate(data, schema, { abortEarly: false, ...options }, err => {
        if (err) {
            return Promise.reject(err);
        }
        return Promise.resolve(null);
    });
}

export default validate;
