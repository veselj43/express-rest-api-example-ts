import './env';

import fs from 'fs';
import cors from 'cors';
import path from 'path';
import helmet from 'helmet';
import morganBody from 'morgan-body';
import express from 'express';
import favicon from 'serve-favicon';
import bodyParser from 'body-parser';
import compression from 'compression';
import * as Sentry from '@sentry/node';

import API_CONFIG from './config/api';
import routes from './routes';
import json from './middlewares/json';
import logger, { logStream } from './utils/logger';
import * as errorHandler from './middlewares/errorHandler';

// Initialize Sentry
// https://docs.sentry.io/platforms/node/express/
Sentry.init({ dsn: process.env.SENTRY_DSN });

// parse console arguments
const APP_ARGS: { [index:string]: string } = {};
process.argv.slice(2).forEach(arg => {
    let [key, value] = arg.split('=');
    key = key.substr(2);
    if (key && value) {
        APP_ARGS[key] = value;
    }
});

const app = express();

const APP_PORT = APP_ARGS.port || (process.env.NODE_ENV === 'test' ? process.env.TEST_APP_PORT : process.env.APP_PORT) || process.env.PORT || '3000';
const APP_HOST = process.env.APP_HOST || '0.0.0.0';
const APP_API_PREFIX = API_CONFIG.v1.api;
const APP_DOCS_PREFIX = API_CONFIG.v1.docs;

const pathToSwaggerUi = require('swagger-ui-dist').absolutePath();

app.set('port', APP_PORT);
app.set('host', APP_HOST);

app.locals.title = process.env.APP_NAME;
app.locals.version = process.env.APP_VERSION;

// This request handler must be the first middleware on the app
app.use(Sentry.Handlers.requestHandler());

app.use(favicon(path.join(__dirname, '/../public', 'favicon.jpg')));
app.use(cors());
app.use(helmet());
app.use(compression());
app.use(bodyParser.json());
morganBody(app, {
    stream: logStream,
    logReqUserAgent: false,
    dateTimeFormat: 'iso'
});
app.use(errorHandler.bodyParser);
app.use(json);

// API Routes
app.use(APP_API_PREFIX, routes);

// Swagger UI
// Workaround for changing the default URL in swagger.json
// https://github.com/swagger-api/swagger-ui/issues/4624
const swaggerIndexContent = fs
    .readFileSync(`${pathToSwaggerUi}/index.html`)
    .toString()
    .replace('https://petstore.swagger.io/v2/swagger.json', APP_API_PREFIX + '/swagger.json');

app.get(APP_DOCS_PREFIX + '/index.html', (req, res) => res.send(swaggerIndexContent));
app.get(APP_DOCS_PREFIX, (req, res) => res.redirect(APP_DOCS_PREFIX + '/index.html'));
app.use(APP_DOCS_PREFIX, express.static(pathToSwaggerUi));

// This error handler must be before any other error middleware
app.use(Sentry.Handlers.errorHandler());

// Error Middlewares
app.use(errorHandler.genericErrorHandler);
app.use(errorHandler.methodNotAllowed);

app.listen(app.get('port'), app.get('host'), () => {
    logger.info(`Server started at http://${app.get('host')}:${app.get('port')}${APP_API_PREFIX}`);
});

// Catch unhandled rejections
process.on('unhandledRejection', err => {
    logger.error('Unhandled rejection ', err);

    try {
        Sentry.captureException(err);
    } catch (err) {
        logger.error('Raven error', err);
    } finally {
        process.exit(1);
    }
});

// Catch uncaught exceptions
process.on('uncaughtException', err => {
    logger.error('Uncaught exception ', err);

    try {
        Sentry.captureException(err);
    } catch (err) {
        logger.error('Raven error', err);
    } finally {
        process.exit(1);
    }
});

export default app;
