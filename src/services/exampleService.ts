import Boom from 'boom';

import Example from '../models/example';

export function getExample(id?: number) {
    if (!id) {
        return [
            {
                id: 1,
                name: 'Jan',
            },
        ];
    }

    if (+id === 1) {
        return new Example({
            id: 1,
            name: 'Jan',
        });
    }

    throw Boom.notFound('Example not found.');
}

export function create(example: Example) {
    return new Example(example);
}

export function update(id: number, example: Example) {
    return new Example({ ...example, id });
}

export function remove(id: number) {
    if (id) {
        return null;
    }

    throw Boom.notFound('Example not found.');
}
