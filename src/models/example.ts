class Example {
    id: number;
    name: string;

    constructor(data: Example) {
        this.id = data.id || 2;
        this.name = data.name;
    }
}

export default Example;
