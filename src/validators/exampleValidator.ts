import { Request, Response } from 'express';
import Joi from 'joi';

import validate from '../utils/validate';
import * as exampleService from '../services/exampleService';

const SCHEMA = {
    name: Joi.string()
        .label('Name')
        .max(90)
        .required(),
};

/**
 * Validate create/update user request.
 *
 * @param   {Object}   req
 * @param   {Object}   res
 * @param   {Function} next
 * @returns {Promise}
 */
function exampleValidator(req: Request, res: Response, next: Function): Promise<any> {
    return validate(req.body, SCHEMA)
        .then(() => next())
        .catch(err => next(err));
}

/**
 * Validate users existence.
 *
 * @param   {Object}   req
 * @param   {Object}   res
 * @param   {Function} next
 */
function findExample(req: Request, res: Response, next: Function) {
    try {
        exampleService.getExample(req.params.id);
        next();
    } catch (err) {
        next(err);
    }
}

export { findExample, exampleValidator };
