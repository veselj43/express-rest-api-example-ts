module.exports = {
    root: true,
    env: {
        es6: true,
        node: true,
        mocha: true,
    },
    extends: ['eslint:recommended'],
    parserOptions: {
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    rules: {
        camelcase: [
            'error',
            {
                properties: 'never',
            },
        ],
        indent: [
            'warn',
            4,
            {
                SwitchCase: 1,
            },
        ],
        'max-len': [
            'warn',
            {
                code: 150,
            },
        ],
        'implicit-arrow-linebreak': ['warn', 'beside'],
        'comma-dangle': ['warn', 'always-multiline'],
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        quotes: ['warn', 'single'],
    },
};
