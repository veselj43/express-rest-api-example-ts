# Express REST API Starter with TS

Based on (but rewritten to TS):
[Express API ES6 Starter](https://github.com/mesaugat/express-api-es6-starter)

Setup comes with:

- TypeScript
- API documentation using [swagger-ui-dist](https://www.npmjs.com/package/swagger-ui) and [swagger-jsdoc](https://www.npmjs.com/package/swagger-jsdoc)
- [ESLint](http://eslint.org/) for code linting
- [Prettier](https://www.npmjs.com/package/prettier) for code formatting
- Request validation using [Joi](https://www.npmjs.com/package/joi)
- Configuration management using [dotenv](https://www.npmjs.com/package/dotenv)
- Logging using [winston](https://www.npmjs.com/package/winston)
- Error reporting using [Sentry](http://npmjs.com/package/@sentry/node)
- Tests using [mocha](https://www.npmjs.com/package/mocha), [supertest](https://www.npmjs.com/package/supertest) and [chai](https://www.npmjs.com/package/chai)

---

## Prerequisites

- [Node.js](https://yarnpkg.com/en/docs/install)
- [Yarn](https://yarnpkg.com/en/docs/install)
- [NPM](https://docs.npmjs.com/getting-started/installing-node)

## Setup

Clone the repository, install the dependencies and get started right away.

    $ yarn   # or npm install

Make a copy of `.env.example` as `.env` and update your application details.

Finally, start the application.

    $ yarn start:dev (For development)
    $ NODE_ENV=production yarn start (For production)

Navigate to http://localhost:8848/api-docs/ to verify installation.


## Tests

    $ yarn test

## Why 8848?

Because the highest point in the world is [8848 metres](https://en.wikipedia.org/wiki/Mount_Everest).
